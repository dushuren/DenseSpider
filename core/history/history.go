package history

import (
	"time"
)

const (
	Pending    = "pending"
	Downloaded = "downloaded"
	Extracted  = "extracted"
)

type History interface {
	GetRegisteredCount() int64
	Register(url string) bool
	Update(url string, state string, lastModifyTime time.Time)
}
