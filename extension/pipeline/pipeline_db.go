package pipeline

import (
	"os"
	"time"

	"dense_spider/core/common/com_interfaces"
	"dense_spider/core/common/page_items"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"

	"errors"
)

const (
	dbtype = "mysql"
)

type Document struct {
	Id           int64
	Title        string `xorm:"text"`
	Domain       string
	Content      string `xorm:"text"`
	Url          string
	Created      time.Time `xorm:"created index"`
	DocPublished time.Time `xorm:"index"`
}

var (
	engine *xorm.Engine
)

func init() {
	SetEngine()
	CreatTables()
}

func ConDb() (*xorm.Engine, error) {
	switch {
	case dbtype == "sqlite":
		return xorm.NewEngine("sqlite3", "./data/sqlite.db")

	case dbtype == "mysql":
		return xorm.NewEngine("mysql", "root:123456@tcp(localhost:3306)/go_spider?charset=utf8")

	case dbtype == "pgsql":
		// "user=postgres password=^%*&^%#@* dbname=pgsql sslmode=disable maxcons=10 persist=true"
		//return xorm.NewEngine("postgres", "host=110.76.39.205 user=postgres password=^%*&^%#@* dbname=pgsql sslmode=disable")
		return xorm.NewEngine("postgres", "user=postgres password=^%*&^%#@* dbname=sdc sslmode=disable")
		//return xorm.NewEngine("postgres", "host=127.0.0.1 port=6432 user=postgres password=^%*&^%#@* dbname=sdc sslmode=disable")
	}
	return nil, errors.New("尚未设定数据库连接")
}

func SetEngine() (*xorm.Engine, error) {

	var err error
	if engine == nil {
		engine, err = ConDb()
	}

	//Engine.Mapper = xorm.SameMapper{}

	engine.ShowDebug = true
	engine.ShowErr = true
	engine.ShowSQL = true
	engine.SetMaxOpenConns(100)
	cacher := xorm.NewLRUCacher(xorm.NewMemoryStore(), 10000)
	engine.SetDefaultCacher(cacher)

	f, err := os.Create("../../logs/sql.log")
	if err != nil {
		println(err.Error())
		panic("sql log error!")
	}
	engine.Logger = xorm.NewSimpleLogger(f)

	return engine, err
}

func CreatTables() error {

	return engine.Sync2(new(Document))
}

type PipelineDb struct {
}

func NewPipelineDb() *PipelineDb {
	return &PipelineDb{}
}
func (this *PipelineDb) Process(items *page_items.PageItems, t com_interfaces.Task) {
	doc := new(Document)
	domain, ok := items.GetItem("Domain")
	if !ok {
		domain = ""
	}
	content, ok := items.GetItem("Content")
	if !ok {
		content = ""
	}
	var docPublishedTime time.Time = time.Now()
	var error = errors.New("时间格式错误")
	docPublished, ok := items.GetItem("DocPublished")
	if ok {
		docPublishedTime, error = time.Parse("2006-01-02 15:04:05", docPublished)
		if error != nil {
			println(error.Error())

		}
	}
	title, ok := items.GetItem("Title")
	if !ok {
		title = ""
	}
	doc = &Document{Domain: domain, Url: items.GetRequest().GetUrl(), DocPublished: docPublishedTime, Title: title, Content: content, Created: time.Now()}
	_, err := engine.Insert(doc)
	if err != nil {
		println(err.Error())
		panic(err)
	}
}
